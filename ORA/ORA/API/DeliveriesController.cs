﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ORA.Models;

namespace ORA.Controllers
{
    [Produces("application/json")]
    [Route("api/Deliveries")]
    public class DeliveriesController : Controller
    {
        private readonly ORAContext _context;

        public DeliveriesController(ORAContext context)
        {
            _context = context;
        }

        // GET: api/Deliveries
        [HttpGet]
        public IEnumerable<Delivery> GetDelivery()
        {
            return _context.Delivery;
        }

        // GET: api/Deliveries/5
        [Route("getdeliverystatus/{deliveryId}")]
        [HttpGet]
        public string GetDeliveryStatus(int deliveryId)
        {
            string result = string.Empty;

            var delivery = _context.Delivery.FirstOrDefault(x => x.Id == deliveryId);

            if (delivery == null)         
                return "Parcel not found";

            if (delivery.IsDelivered == true)
            {
                var pickup = _context.Person.FirstOrDefault(x => x.Id == delivery.PickUpBy);
                return pickup.Name;
            }
            
            return result;
        }


        [Route("acceptparcel/{id}")]
        [HttpPost]
        public string AcceptParcel([FromRoute] int id)
        {
            string result = string.Empty;

            var delivery = _context.Delivery.FirstOrDefault(x => x.Id == id);

            if (delivery == null)
                return "Parcel not found";

            delivery.IsDelivered = true;
            delivery.PickUpBy = 1;

            _context.SaveChanges();

            return result;
        }

        [Route("AcceptParcelByIC/{deliveryId}/{userId}")]
        [HttpPost]
        public async Task<IActionResult> AcceptParcelByIC(int deliveryId, int userId)
        {
            string result = string.Empty;

            var delivery = _context.Delivery.FirstOrDefault(x => x.Id == deliveryId);

            if (delivery == null)
                return BadRequest(new { errorMsg = "Parcel not found" });

            delivery.IsDelivered = true;
            delivery.PickUpBy = userId;

            _context.SaveChanges();

            return Ok(new { Msg = "Delivery updated." });
        }


        // GET: api/Deliveries/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDelivery([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var delivery = await _context.Delivery.SingleOrDefaultAsync(m => m.Id == id);

            if (delivery == null)
            {
                return NotFound();
            }

            return Ok(delivery);
        }

        // PUT: api/Deliveries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDelivery([FromRoute] int id, [FromBody] Delivery delivery)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != delivery.Id)
            {
                return BadRequest();
            }

            _context.Entry(delivery).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DeliveryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Deliveries
        [HttpPost]
        public async Task<IActionResult> PostDelivery([FromBody] Delivery delivery)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Delivery.Add(delivery);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DeliveryExists(delivery.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDelivery", new { id = delivery.Id }, delivery);
        }

        // DELETE: api/Deliveries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDelivery([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var delivery = await _context.Delivery.SingleOrDefaultAsync(m => m.Id == id);
            if (delivery == null)
            {
                return NotFound();
            }

            _context.Delivery.Remove(delivery);
            await _context.SaveChangesAsync();

            return Ok(delivery);
        }

        private bool DeliveryExists(int id)
        {
            return _context.Delivery.Any(e => e.Id == id);
        }
    }
}