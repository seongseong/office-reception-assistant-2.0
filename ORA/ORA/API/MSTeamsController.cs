﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using ORA.Models;
using Microsoft.EntityFrameworkCore;

namespace ORA.Controllers
{
    [Produces("application/json")]
    [Route("api/MSTeams")]
    public class MSTeamsController : Controller
    {

        private readonly ORAContext _context;

        public MSTeamsController(ORAContext context)
        {
            _context = context;
        }


        // GET: api/MSTeams
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MSTeams/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MSTeams
        [HttpPost("{id}")]
        public async Task PostMSTeam([FromRoute]int id)
        {
            var delivery = await _context.Delivery.SingleOrDefaultAsync(m => m.Id == id);

            if (delivery != null)
            {
                var person = await _context.Person.SingleOrDefaultAsync(m => m.Id == delivery.Recipient);


                //MS Teams card template - load from txt file
                string contents = System.IO.File.ReadAllText(@"card.txt");

                //parameters sent from kiosk api
                string name = person.Name;
                string mobile = person.Mobile;

                string delegates = string.Empty;
                List<string> list = new List<string>();

                var g = _context.Delegate.Where(x => x.PersonId == person.Id).Select(x => x.DelegatePersonId).ToList();

                foreach (var d in g)
                {
                    if (d.HasValue && _context.Person.FirstOrDefault(x => x.Id == d.Value) != null)
                        list.Add(_context.Person.FirstOrDefault(x => x.Id == d.Value).Name);
                }
                delegates = string.Join(", ", list);
                //end

                //TODO: this url to open accept delivery page - open from MS Teams button
                string urlz = string.Format(@"https://officereceptionassistant.azurewebsites.net/Kiosk/AcceptParcel?id={0}", id);

                contents = contents.Replace("{@name}", name).Replace("{@mobile}", mobile).Replace("{@delegates}", delegates).Replace("{@url}", urlz);

                var client = new HttpClient();
                var url = @"https://outlook.office.com/webhook/f54e79a0-3e9c-41eb-b2cb-1e32b75700e5@1ca8bd94-3c97-4fc6-8955-bad266b43f0b/IncomingWebhook/3b41df005c4745bc8062e1b4cbddba88/27104d89-4f76-412d-9bf9-933a678624bc";
                //ConfigurationManager.AppSettings["incomingWebhook"];
                //var bodyzz = @"\";
                // var message = "{\"text\": \"" + bodyzz + "\"}";
                var message = contents;
                var body = new StringContent(message, System.Text.Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, body);//
            }
        }

        // POST: api/MSTeams/vistor/1
        [Route("vistor/{id}")]
        [HttpPost]
        public async Task PostMSTeamVisitor([FromRoute]int id)
        {
            var visitor = await _context.VisitorLogBook.SingleOrDefaultAsync(m => m.Id == id);

            if (visitor != null)
            {
                var person = await _context.Person.SingleOrDefaultAsync(m => m.Id == visitor.PersonId);

                //MS Teams card template - load from txt file
                string contents = System.IO.File.ReadAllText(@"cardVisitor.txt");

                //parameters sent from kiosk api
                string name = person.Name;
                string visitorName = visitor.VisitorName;

                //end


                contents = contents.Replace("{@name}", name).Replace("{@visitorName}", visitorName);

                var client = new HttpClient();
                var url = @"https://outlook.office.com/webhook/f54e79a0-3e9c-41eb-b2cb-1e32b75700e5@1ca8bd94-3c97-4fc6-8955-bad266b43f0b/IncomingWebhook/3b41df005c4745bc8062e1b4cbddba88/27104d89-4f76-412d-9bf9-933a678624bc";
                //ConfigurationManager.AppSettings["incomingWebhook"];
                //var bodyzz = @"\";
                // var message = "{\"text\": \"" + bodyzz + "\"}";
                var message = contents;
                var body = new StringContent(message, System.Text.Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, body);//
            }
        }

        // PUT: api/MSTeams/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
