﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ORA.Models;
using Delegate = ORA.Models.Delegate;


namespace ORA.Controllers
{
    public class EmployeeProfileController : Controller
    {
        private readonly ORAContext _context;

        public EmployeeProfileController(ORAContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var employeeProfile = getEmployeeProfile(1);

            return View(employeeProfile);
        }

        public async Task<ActionResult> Edit(int employeeId)
        {
            var employeeProfile = getEmployeeProfile(employeeId);

            return View("EmployeeProfileEdit", employeeProfile);
        }

        public ActionResult New (int employeeId)
        {
            var employeeNewParcel = new Delivery();
            employeeNewParcel.Recipient = employeeId;

            return View("EmployeeParcelAdd", employeeNewParcel);
        }

        public ActionResult Insert(Delivery newDelivery)
        {
            var employeeNewParcel = new Delivery();
            employeeNewParcel.TrackingNumber = newDelivery.TrackingNumber;
            employeeNewParcel.Recipient = newDelivery.Recipient;
            employeeNewParcel.IsDelivered = false;

            try
            {
                _context.Delivery.Add(employeeNewParcel);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            var employeeProfile = getEmployeeProfile(newDelivery.Recipient.Value);

            return View("Index", employeeProfile);
        }

        public ActionResult Update(EmployeeProfile employeeProfile)
        {
            var todayDate = System.DateTime.Today.ToShortDateString();
            var employeeDetails = _context.Person.FirstOrDefault(x => x.Id == employeeProfile.EmployeeId);
            employeeDetails.Mobile = employeeProfile.EmployeeDetails.Mobile;
            employeeDetails.Ic = employeeProfile.EmployeeDetails.Ic;

            var delegation = new Delegate()
            {
                PersonId = employeeProfile.EmployeeId,
                DelegatePersonId = employeeProfile.EmployeeDelegationId
            };

            var employeeDailyStatus = new DailyStatus()
            {
                PersonId = employeeProfile.EmployeeId,
                Date = System.DateTime.Today,
                Status = employeeProfile.EmployeeDailyStatus.Status
            };

            try
            {
                _context.Person.Update(employeeDetails);
                _context.DailyStatus.Add(employeeDailyStatus);
                _context.Delegate.Update(delegation);
                _context.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            var employeeProfileUpdated = getEmployeeProfile(employeeProfile.EmployeeId);

            return View("Index",employeeProfileUpdated);
        }

        private EmployeeProfile getEmployeeProfile(int employeeId)
        {
            var todayDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var employeeDetails = _context.Person.FirstOrDefault(x => x.Id == employeeId);
            var employeeDailyStatus = _context.DailyStatus.FirstOrDefault(x => x.Id == employeeId && x.Date == todayDate);
            var employeeDeliveries = new List<Delivery>();
            var deliveries = _context.Delivery.Where(x => x.Recipient == employeeId);
            foreach (var delivery in deliveries)
            {
                employeeDeliveries.Add(delivery);
            }

            var delegations = _context.Delegate.Where(x => x.PersonId == employeeId).Select(y => y.DelegatePersonId).Last();
            var employeeDelegationName = getEmployeeName(delegations.Value);
            var employeeDelegationId = delegations.Value;

            var employeeProfile = new EmployeeProfile()
            {
                EmployeeId = employeeId,
                EmployeeDetails = employeeDetails,
                EmployeeDailyStatus = employeeDailyStatus,
                EmployeeDeliveries = employeeDeliveries,
                EmployeeDelegationId = employeeDelegationId,
                EmployeeDelegationName = employeeDelegationName
            };
            return employeeProfile;
        }

        private string getEmployeeName(int employeeId)
        {
            var employeeName = _context.Person.FirstOrDefault(x => x.Id == employeeId).Name;
            return employeeName;
        }
    }
}