﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ORA.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ORA.Controllers
{
    public class KioskController : Controller
    {
        private readonly ORAContext _context;

        public KioskController(ORAContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Parcel()
        {
            //ParcelOrder order = new ParcelOrder();
            //order.Deliveries = await _context.Delivery.ToListAsync();
            //order.People = await _context.Person.ToListAsync();

            return View(await _context.Delivery.ToListAsync());
        }

        public IActionResult AcceptParcel()
        {
            return View();
        }

        [Route("Timer/{userId}/{deliveryId}")]
        public async Task<IActionResult> Timer(int userId, int deliveryId)
        {
            ViewData["userId"] = userId;
            ViewData["deliveryId"] = deliveryId;

            MSTeamsController team = new MSTeamsController(_context);
            await team.PostMSTeam(deliveryId);

            return View();
        }

        [Route("TimeOut/{deliveryId}/{userId}")]
        public IActionResult TimeOut(int deliveryId, int userId)
        {
            ViewData["userId"] = userId;
            ViewData["deliveryId"] = deliveryId;

            return View();
        }
        public IActionResult Receive()
        {
            return View();
        }

        [Route("Reveal/{userId}/{deliveryId}")]
        public IActionResult Reveal(int userId, int deliveryId)
        {
            ViewData["userId"] = userId;
            ViewData["deliveryId"] = deliveryId;

            return View(_context.Person.SingleOrDefault(x => x.Id.Equals(userId)));
        }
    }
}
