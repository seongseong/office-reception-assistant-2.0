﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ORA.Models;
using System.Net.Http;

namespace ORA.Controllers
{
    public class VisitorController : Controller
    {
        private readonly ORAContext _context;

        public VisitorController(ORAContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var visitors = _context.VisitorLogBook.ToList();
            var visitorList = new List<EmployeeVisitor>();
            foreach (var visitor in visitors)
            {
                visitorList.Add(new EmployeeVisitor()
                {
                    EmployeeName = getEmployeeName(visitor.PersonId.Value),
                    VisitorLogBook = visitor
                });   
            }
            
            return View(visitorList);
        }

        public void PostMSTeamVisitor(int id)
        {
            var visitor = _context.VisitorLogBook.SingleOrDefault(m => m.Id == id);

            if (visitor != null)
            {
                var person = _context.Person.SingleOrDefault(m => m.Id == visitor.PersonId);

                //MS Teams card template - load from txt file
                string contents = System.IO.File.ReadAllText(@"cardVisitor.txt");

                //parameters sent from kiosk api
                string name = person.Name;
                string visitorName = visitor.VisitorName;

                //end


                contents = contents.Replace("{@name}", name).Replace("{@visitorName}", visitorName);

                var client = new HttpClient();
                var url = @"https://outlook.office.com/webhook/f54e79a0-3e9c-41eb-b2cb-1e32b75700e5@1ca8bd94-3c97-4fc6-8955-bad266b43f0b/IncomingWebhook/3b41df005c4745bc8062e1b4cbddba88/27104d89-4f76-412d-9bf9-933a678624bc";
                //ConfigurationManager.AppSettings["incomingWebhook"];
                //var bodyzz = @"\";
                // var message = "{\"text\": \"" + bodyzz + "\"}";
                var message = contents;
                var body = new StringContent(message, System.Text.Encoding.UTF8, "application/json");
                var result = client.PostAsync(url, body);//
            }
        }


        public ActionResult Insert(VisitorLogBook visitorLogBook)
        {
            try
            {
                _context.VisitorLogBook.Add(visitorLogBook);
                _context.SaveChanges();

                var id = _context.VisitorLogBook.OrderByDescending(x => x.Id).First().Id;
               PostMSTeamVisitor(id);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            //var visitors = _context.VisitorLogBook.ToList();
            //var visitorList = new List<EmployeeVisitor>();
            //foreach (var visitor in visitors)
            //{
            //    visitorList.Add(new EmployeeVisitor()
            //    {
            //        EmployeeName = getEmployeeName(visitor.PersonId.Value),
            //        VisitorLogBook = visitor
            //    });   
            //}
           

           return View("~/Views/Kiosk/Index.cshtml");
        }

        public ActionResult New()
        {
            return View("VisitorRegister");
        }

        private string getEmployeeName(int employeeId)
        {
            var employeeName = _context.Person.FirstOrDefault(x => x.Id == employeeId).Name;
            return employeeName;
        }
    }
}