﻿using System;
using System.Collections.Generic;

namespace ORA.Models
{
    public partial class DailyStatus
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
    }
}
