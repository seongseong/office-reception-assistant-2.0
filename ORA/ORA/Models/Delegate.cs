﻿using System;
using System.Collections.Generic;

namespace ORA.Models
{
    public partial class Delegate
    {
        public int Id { get; set; }
        public int? PersonId { get; set; }
        public int? DelegatePersonId { get; set; }
    }
}
