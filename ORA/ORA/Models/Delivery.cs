﻿using System;
using System.Collections.Generic;

namespace ORA.Models
{
    public partial class Delivery
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; }
        public int? Recipient { get; set; }
        public bool? IsDelivered { get; set; }
        public int? PickUpBy { get; set; }
    }
}
