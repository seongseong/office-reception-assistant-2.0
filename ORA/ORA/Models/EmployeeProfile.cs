﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ORA.Models
{
    public class EmployeeProfile
    {
        public int EmployeeId { get; set; }
        public Person EmployeeDetails { get; set; }
        public DailyStatus EmployeeDailyStatus { get; set; }
        public List<Delivery> EmployeeDeliveries { get; set; }
        public int EmployeeDelegationId { get; set; }
        public string EmployeeDelegationName { get; set; }
    }
}
