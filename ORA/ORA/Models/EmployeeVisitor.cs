﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ORA.Models
{
    public class EmployeeVisitor
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public VisitorLogBook VisitorLogBook { get; set; }
    }
}
