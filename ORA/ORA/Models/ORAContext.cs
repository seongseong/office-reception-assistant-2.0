﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ORA.Models
{
    public partial class ORAContext : DbContext
    {
        public virtual DbSet<DailyStatus> DailyStatus { get; set; }
        public virtual DbSet<Delegate> Delegate { get; set; }
        public virtual DbSet<Delivery> Delivery { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<VisitorLogBook> VisitorLogBook { get; set; }

        public ORAContext(DbContextOptions<ORAContext> options) : base(options) { }
        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer(@"Data Source=tcp:ora2.database.windows.net,1433;Initial Catalog=ORA;Integrated Security=False;User Id=ora_admin;Password=Password123;Encrypt=True;TrustServerCertificate=False;MultipleActiveResultSets=True");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DailyStatus>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Delivery>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .HasName("IX_Delivery");

                entity.Property(e => e.TrackingNumber).HasMaxLength(50);
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(320)
                    .IsUnicode(false);

                entity.Property(e => e.Ic)
                    .HasColumnName("IC")
                    .HasMaxLength(50);

                entity.Property(e => e.Mobile).HasMaxLength(50);
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.Status1)
                    .IsRequired()
                    .HasColumnName("Status")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VisitorLogBook>(entity =>
            {
                entity.Property(e => e.VisitorIc)
                    .HasColumnName("VisitorIC")
                    .HasMaxLength(50);

                entity.Property(e => e.VisitorMobile).HasMaxLength(50);

                entity.Property(e => e.VisitorName).HasMaxLength(50);
            });
        }
    }
}
