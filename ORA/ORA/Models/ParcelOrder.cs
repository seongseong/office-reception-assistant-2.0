﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ORA.Models
{
    public class ParcelOrder
    {
        public List<Delivery> Deliveries { get; set; }
        public List<Person> People { get; set; }
    }

}
