﻿using System;
using System.Collections.Generic;

namespace ORA.Models
{
    public partial class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Ic { get; set; }
        public string Email { get; set; }
    }
}
