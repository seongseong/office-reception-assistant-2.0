﻿using System;
using System.Collections.Generic;

namespace ORA.Models
{
    public partial class VisitorLogBook
    {
        public int Id { get; set; }
        public string VisitorName { get; set; }
        public string VisitorIc { get; set; }
        public string VisitorMobile { get; set; }
        public int? PersonId { get; set; }
    }
}
